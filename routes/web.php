<?php
use Illuminate\Http\File;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('simulator');
});
Route::get('download/template', function () {
	$template = new File(public_path() . '/Template Fisica 3 Proyecto.xlsx', 'Template.xlsx');

	return response()->download($template);
});
