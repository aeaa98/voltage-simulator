<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Simulador Fisica 3</title>
        {{-- <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}"> --}}

	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
	<body>
	    <div class="container-fluid" id="app">
			<example-component/>
	    </div>
	</body>
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="{{ mix('js/app.js') }}" defer></script>
</html>
